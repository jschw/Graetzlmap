package com.example.Graetzlmap.dTOs;
import com.example.Graetzlmap.enums.Categories;

public class UpdateMapPostDTO {

    private String title;
    private String text;
    private String picturePath;
    private Categories category;
    private Integer postalCode;
    private String username;
    private int postId;

    public UpdateMapPostDTO(String title, String text, String picturePath, Categories category, String username, int postId) {
        this.title = title;
        this.text = text;
        this.picturePath = picturePath;
        this.category = category;
        this.username = username;
        this.postId = postId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPicturePath() {
        return picturePath;
    }

    public void setPicturePath(String picturePath) {
        this.picturePath = picturePath;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
