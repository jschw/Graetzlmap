package com.example.Graetzlmap.dTOs;

public class ChangeUserSettingsDTO {
    private String newUsername;
    private String password;
    private String newPassword;
    private String email;
    private Integer postalCode;
    private String image;

    public ChangeUserSettingsDTO(String newUsername, String password, String newPassword, String email, Integer postalCode, String image) {
        this.newUsername = newUsername;
        this.password = password;
        this.newPassword = newPassword;
        this.email = email;
        this.postalCode = postalCode;
        this.image = image;
    }

    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
