package com.example.Graetzlmap.dTOs;

public class CommentOfPostDTO {

    private String text;
    private int postId;

    public CommentOfPostDTO(String text, int postId) {
        this.text = text;
        this.postId = postId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
