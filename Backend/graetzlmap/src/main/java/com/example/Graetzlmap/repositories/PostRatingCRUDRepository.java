package com.example.Graetzlmap.repositories;

import com.example.Graetzlmap.model.PostRating;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRatingCRUDRepository extends CrudRepository<PostRating, Integer> {
    PostRating findByPersonIdAndPostId(int personId, int postId);
    Boolean existsByPersonIdAndPostId(int personId, int postId);
}
