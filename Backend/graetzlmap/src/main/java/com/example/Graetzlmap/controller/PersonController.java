package com.example.Graetzlmap.controller;
import com.example.Graetzlmap.dTOs.ChangeUserSettingsDTO;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.services.ImageUploadService;
import com.example.Graetzlmap.services.PersonService;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class PersonController {
    @Autowired
    PersonService personService;
    @Autowired
    ImageUploadService imageUploadService;

    @PutMapping("/changeSettings")
    public Person changeUserSettings(@RequestBody ChangeUserSettingsDTO changeUserSettingsDTO) throws ResponseStatusException{
        return personService.changeSettings(changeUserSettingsDTO);}

    @GetMapping("/all")
    public List<Person> returnAllUsers() {
        List<Person> p1 =  personService.findAll();
        p1.forEach(x -> x.setPassword(null));
        return p1;
    }


    @GetMapping("/{username}")
    public Person returnCertainUser(@PathVariable String username) throws ResponseStatusException{return personService.findPerson(username);}

    @DeleteMapping("/{username}")
    public void deleteUserWithUsername(@PathVariable String username) throws ResponseStatusException{personService.deleteByUsername(username);}

    // TITLE                    IMAGE UPLOAD

    @PostMapping("/profilePicture")
    public Person saveProfilePicture(@RequestParam ("image") @NotNull MultipartFile multipartFile) throws Exception{
        return imageUploadService.saveProfilePicture(multipartFile);
    }

}
