package com.example.Graetzlmap;
import com.example.Graetzlmap.dummyData.DummyMethods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.Transactional;


@SpringBootApplication
public class GraetzlmapApplication implements CommandLineRunner {
	@Autowired
	DummyMethods dummyMethods;
	public static void main(String[] args) {
		SpringApplication.run(GraetzlmapApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		try{
			dummyMethods.generateMods();
			dummyMethods.generate3Admins();
			dummyMethods.createAccurateDummyPosts();
			dummyMethods.generateEndUser();
			dummyMethods.personaDummyPosts();
			dummyMethods.randomLikingDisliking();
			dummyMethods.commentPosts();
			dummyMethods.createUnaprovedComments();
		}catch (Exception e){
			throw e;
		}
	}
}
