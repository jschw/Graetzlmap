package com.example.Graetzlmap.services;
import com.example.Graetzlmap.dTOs.NewMapPostDTO;
import com.example.Graetzlmap.dTOs.UpdateMapPostDTO;
import com.example.Graetzlmap.enums.Roles;
import com.example.Graetzlmap.exitDTOs.MapPostWithCommentsDTO;
import com.example.Graetzlmap.model.CommentOfPost;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.model.Post;
import com.example.Graetzlmap.repositories.MapPostCRUDRepository;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.validator.InputValidator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
@Service
public class MapPostService {
    @Autowired
    MapPostCRUDRepository mapPostCRUDRepository;
    @Autowired
    PersonService personService;
    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    InputValidator validator;

    public MapPostWithCommentsDTO mapPostWithApprovedComments(Integer id){
        MapPost mapPost = findById(id);
        List<CommentOfPost> commentOfPostList = mapPost.getCommentOfPostList().stream().filter(m -> m.isApprovalStatus()).toList();
        MapPostWithCommentsDTO mapPostWithCommentsDTO = new MapPostWithCommentsDTO(mapPost,commentOfPostList);
        return mapPostWithCommentsDTO;
    }

    public List<MapPost> findAll(){
        return (List<MapPost>) mapPostCRUDRepository.findAll();
    }

    public List<MapPost> findAllApprovedPosts(){
        return findAll().stream()
                .filter(Post::isApprovalStatus)
                .toList();
    }

    public List<MapPost> findAllIllegalPosts(){
        return findAll().stream()
                .filter(m -> !m.isApprovalStatus())
                .toList();
    }

    public Integer newMapPost(@NotNull NewMapPostDTO newMapPostDTO) throws ResponseStatusException {
        String userName = validator.getCurrentUsername();
        validator.validateNewMapPost(newMapPostDTO);
        try {
            LocalDateTime date = LocalDateTime.now();
            MapPost mapPost = new MapPost(
                    newMapPostDTO.getTitle(),
                    newMapPostDTO.getText(),
                    newMapPostDTO.getCategory(),
                    date,
                    newMapPostDTO.getPostalCode(),
                    newMapPostDTO.getLatitude(),
                    newMapPostDTO.getLongitude()
            );
            Person person = personService.findPerson(userName);
            if(person.getRole() != Roles.ENDUSER)
                mapPost.setApprovalStatus(true);
            person.addPostSet(mapPost);
            mapPost.setPerson(person);
            mapPostCRUDRepository.save(mapPost);
            return mapPost.getId();
        }catch (Exception e){
            e.getMessage();
            return null;
        }
    }

    public List<MapPost> findPostsByUser(String username) throws ResponseStatusException{
        validator.validateUsername(username);
        return findAll().stream()
                .filter(m -> m.getUsername().equals(username))
                .toList();
    }

    //  NOT IN USE BECAUSE FRONTEND DID NOT GET IT DONE
    public void updateMapPost(@NotNull UpdateMapPostDTO updateMapPostDTO) throws ResponseStatusException {
        validator.validateMapPostExistence(updateMapPostDTO.getPostId());
        validator.validateUsername(SecurityContextHolder.getContext().getAuthentication().getName());
        MapPost mapPost = mapPostCRUDRepository.findById(updateMapPostDTO.getPostId()).get();
        Person person = personCRUDRepository.findByUsername(updateMapPostDTO.getUsername());
        if(mapPost.getPerson() != person)
            throw new ResponseStatusException(HttpStatus.CONFLICT, "user did not create the post");
        try {
            if (updateMapPostDTO.getTitle() != null && !updateMapPostDTO.getTitle().equals(""))
                mapPost.setTitle(updateMapPostDTO.getTitle());
            if(updateMapPostDTO.getText() != null && !updateMapPostDTO.getText().equals(""))
                mapPost.setText(updateMapPostDTO.getText());
            if(updateMapPostDTO.getPicturePath() != null && !updateMapPostDTO.getPicturePath().equals(""))
                mapPost.setImage(updateMapPostDTO.getPicturePath());
            if(updateMapPostDTO.getCategory() != null)
                mapPost.setCategory(updateMapPostDTO.getCategory());
            if(personCRUDRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()).getRole() != Roles.ENDUSER)
                mapPost.setApprovalStatus(true);
            else
                mapPost.setApprovalStatus(false);
            mapPostCRUDRepository.save(mapPost);
        }catch (Exception e){
            e.getMessage();
        }
    }

    public MapPost findById(Integer id) throws ResponseStatusException{
        validator.validateMapPostExistence(id);
        return mapPostCRUDRepository.findById(id).get();
    }

    public MapPostWithCommentsDTO getMapPostAllComments(Integer id){
        validator.validateMapPostExistence(id);
        MapPost mapPost = findById(id);
        List<CommentOfPost> commentOfPostList = mapPost.getCommentOfPostList();
        MapPostWithCommentsDTO mapPostWithCommentsDTO = new MapPostWithCommentsDTO(mapPost, commentOfPostList);
        return mapPostWithCommentsDTO;
    }

    public void deleteMapPost(Integer id) throws ResponseStatusException{
        validator.validateMapPostExistence(id);
        mapPostCRUDRepository.deleteById(id);
    }

    public void changeApproval(Integer id) throws ResponseStatusException{
        String currentAdminMod = validator.getCurrentUsername();
        validator.validateMapPostExistence(id);
        MapPost mapPost = mapPostCRUDRepository.findById(id).get();
        mapPost.setApprovalStatus(!mapPost.isApprovalStatus());
        mapPost.setApprovalModerator(currentAdminMod);
        mapPostCRUDRepository.save(mapPost);
    }
}
