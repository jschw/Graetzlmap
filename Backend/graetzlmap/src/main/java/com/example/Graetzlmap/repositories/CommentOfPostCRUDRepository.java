package com.example.Graetzlmap.repositories;

import com.example.Graetzlmap.model.CommentOfPost;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentOfPostCRUDRepository extends CrudRepository<CommentOfPost, Integer> {
}
