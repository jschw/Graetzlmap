package com.example.Graetzlmap.services;
import com.example.Graetzlmap.dTOs.CommentOfPostChangeDTO;
import com.example.Graetzlmap.dTOs.CommentOfPostDTO;
import com.example.Graetzlmap.enums.Roles;
import com.example.Graetzlmap.model.CommentOfPost;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.repositories.CommentOfPostCRUDRepository;
import com.example.Graetzlmap.repositories.MapPostCRUDRepository;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.validator.InputValidator;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import java.time.LocalDateTime;
import java.util.List;

@Transactional
@Service
public class CommentOfPostService {
    @Autowired
    CommentOfPostCRUDRepository commentOfPostCRUDRepository;
    @Autowired
    MapPostCRUDRepository mapPostCRUDRepository;
    @Autowired
    PersonService personService;
    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    InputValidator validator;

    public List<CommentOfPost> findAll(){return (List<CommentOfPost>) commentOfPostCRUDRepository.findAll();}

    public CommentOfPost createNewCommentOfPost(@NotNull CommentOfPostDTO commentOfPostDTO) throws ResponseStatusException{
        String userName = validator.getCurrentUsername();
        validator.validateMapPostExistence(commentOfPostDTO.getPostId());
        try{
            LocalDateTime date = LocalDateTime.now();
            boolean approval = true;
            if(personService.findPerson(userName).getRole() == Roles.ENDUSER)
                approval = false;
            CommentOfPost commentOfPost = new CommentOfPost(commentOfPostDTO.getText(), approval, date);
            commentOfPostCRUDRepository.save(commentOfPost);
            MapPost mapPost = mapPostCRUDRepository.findById(commentOfPostDTO.getPostId()).get();
            commentOfPost.setPost(mapPost);
            mapPost.addCommentOfPost(commentOfPost);
            mapPostCRUDRepository.save(mapPost);
            Person person = personService.findPerson(userName);
            commentOfPost.setPerson(person);
            person.addCommentOfPostSet(commentOfPost);
            personCRUDRepository.save(person);
            commentOfPostCRUDRepository.save(commentOfPost);
            return commentOfPost;
        }catch (Exception e) {
            e.getMessage();
            throw e;
        }
    }

    public void changeApproval(Integer id) throws ResponseStatusException {
        String currentAdminMod = validator.getCurrentUsername();
        validator.validatePostCommentExistence(id);
        try {
            CommentOfPost commentOfPost = commentOfPostCRUDRepository.findById(id).get();
            commentOfPost.setApprovalStatus(!commentOfPost.isApprovalStatus());
            commentOfPost.setApprovalModerator(currentAdminMod);
            commentOfPostCRUDRepository.save(commentOfPost);
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    //          Not in use because Frontend is not far enough
    public void updateCommentOfPost(@NotNull CommentOfPostChangeDTO commentOfPostChangeDTO) throws ResponseStatusException{
        validator.validatePostCommentExistence(commentOfPostChangeDTO.getCommentId());
        try {
            CommentOfPost commentOfPost = commentOfPostCRUDRepository.findById(commentOfPostChangeDTO.getCommentId()).get();
            commentOfPost.setText(commentOfPostChangeDTO.getNewText());
            commentOfPostCRUDRepository.save(commentOfPost);
        }catch (Exception e){
            e.getMessage();
            throw e;
        }
    }

    public CommentOfPost findCommentOfPost(Integer id) throws ResponseStatusException{
        validator.validatePostCommentExistence(id);
        try{
            return commentOfPostCRUDRepository.findById(id).get();
        }catch (Exception e){
            e.getMessage();
            throw e;
        }
    }

    public void deleteCommentOfPost(Integer id) throws ResponseStatusException{
        validator.validatePostCommentExistence(id);
        try{
            commentOfPostCRUDRepository.deleteById(id);
        }catch (Exception e){
            e.getMessage();
            throw e;
        }
    }

    public List<CommentOfPost> fetchIllegalComments(){
        return findAll().stream()
                .filter(c -> !c.isApprovalStatus())
                .toList();
    }
}
