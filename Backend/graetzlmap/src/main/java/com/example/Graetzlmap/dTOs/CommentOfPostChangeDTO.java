package com.example.Graetzlmap.dTOs;

public class CommentOfPostChangeDTO {
    private String newText;
    private int commentId;

    public CommentOfPostChangeDTO(String newText, int commentId) {
        this.newText = newText;
        this.commentId = commentId;
    }

    public String getNewText() {
        return newText;
    }

    public void setNewText(String newText) {
        this.newText = newText;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }
}
