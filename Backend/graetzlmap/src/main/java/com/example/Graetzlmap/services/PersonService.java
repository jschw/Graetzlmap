package com.example.Graetzlmap.services;
import com.example.Graetzlmap.dTOs.ChangeUserSettingsDTO;
import com.example.Graetzlmap.enums.Roles;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import com.example.Graetzlmap.validator.InputValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class PersonService {
    @Autowired
    PersonCRUDRepository personCRUDRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    HttpServletRequest httpServletRequest;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    InputValidator validator;

    //  TITLE                   PRIVATE METHODS - needed for PersonService.changeSettings() (below)

    private void changeCookie(String username ,String password){
        String email = personCRUDRepository.findByUsername(username).getEmail();
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(email, password);
        authToken.setDetails(new WebAuthenticationDetails(httpServletRequest));
        Authentication authentication = authenticationManager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        httpServletRequest.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, SecurityContextHolder.getContext());
    }

    private void changeUsername(String username, String newUsername, String password){
        validator.validateUsernameUniqueness(newUsername);
        if(!personCRUDRepository.existsByUsername(newUsername)){
            validator.checkUsernameWithPassword(username, password);
            Person person = findPerson(username);
            person.setUsername(newUsername);
            personCRUDRepository.save(person);
            changeCookie(newUsername, password);
        }
    }

    private void changePassword(String username, String password, String newPassword){
        validator.checkUsernameWithPassword(username, password);
        Person person = personCRUDRepository.findByUsername(username);
        person.setPassword(passwordEncoder.encode(newPassword));
        personCRUDRepository.save(person);
        changeCookie(username, newPassword);
    }

    private void changeEmail(String email){
        validator.validateEmailUniqueness(email);
        Person person = personCRUDRepository.findByUsername(validator.getCurrentUsername());
        person.setEmail(email);
        personCRUDRepository.save(person);
    }

    private void changePostalCode(Integer postalCode){
        validator.validatePostalCodeNotNull(postalCode);
        Person person = personCRUDRepository.findByUsername(validator.getCurrentUsername());
        person.setPostalCode(postalCode);
        personCRUDRepository.save(person);
    }

    //  TITLE                   PUBLIC METHODS

    //  Exported methods because for better readability
    //      gets complex because:
    //          if username or password gets changed -> cookie needs to be resent.
    public Person changeSettings(ChangeUserSettingsDTO changeUserSettingsDTO) throws ResponseStatusException{
        try {
            String username = validator.getCurrentUsername();
            Person person = personCRUDRepository.findByUsername(username);
            if(validator.validateString(changeUserSettingsDTO.getNewUsername()))
                changeUsername(username,changeUserSettingsDTO.getNewUsername(), changeUserSettingsDTO.getPassword());
            if(validator.validateString(changeUserSettingsDTO.getNewPassword()))
                changePassword(validator.getCurrentUsername(), changeUserSettingsDTO.getPassword(), changeUserSettingsDTO.getNewPassword());
            if(validator.validateString(changeUserSettingsDTO.getEmail()))
                changeEmail(changeUserSettingsDTO.getEmail());
            if(changeUserSettingsDTO.getPostalCode() != null)
                changePostalCode(changeUserSettingsDTO.getPostalCode());
            return person;
        }catch (Exception e){
            throw e;
        }
    }

    public void deleteByUsername(String username) throws ResponseStatusException{
        validator.validateUsername(username);
        personCRUDRepository.delete(personCRUDRepository.findByUsername(username));
    }

    public Person findPerson(String username) throws ResponseStatusException{
        validator.validateUsername(username);
        return personCRUDRepository.findByUsername(username);
    }

    public List<Person> findAll(){return (List<Person>) personCRUDRepository.findAll();}

    //  TITLE               Role-Requests

    public void personToEndUser(String username) throws ResponseStatusException{
        validator.validateUsername(username);
        try {
            Person person = personCRUDRepository.findByUsername(username);
            person.setRole(Roles.ENDUSER);
            personCRUDRepository.save(person);
        }catch (Exception e){
            throw e;
        }
    }

    public void personToModerator(String username) throws ResponseStatusException {
        validator.validateUsername(username);
        try {
            Person person = personCRUDRepository.findByUsername(username);
            person.setRole(Roles.MODERATOR);
            personCRUDRepository.save(person);
        }catch (Exception e){
            throw e;
        }
    }

    public void personToAdmin(String username) throws ResponseStatusException{
        validator.validateUsername(username);
        try {
            Person person = personCRUDRepository.findByUsername(username);
            person.setRole(Roles.ADMIN);
            personCRUDRepository.save(person);
        }catch (Exception e){
            throw e;
        }
    }
}
