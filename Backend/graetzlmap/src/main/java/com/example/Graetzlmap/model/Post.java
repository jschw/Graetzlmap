package com.example.Graetzlmap.model;
import com.example.Graetzlmap.enums.Categories;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "post")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private int id;

    @Column(nullable = false, length = 10000)
    private String title;

    @Column(nullable = false)
    private String text;

    @Column
    private String image;

    @Column
    private boolean approvalStatus;

    @Column
    private String approvalModerator;

    @Column
    private int likes;

    @Column
    private int dislikes;

    @Column
    private Categories category;

    @Column(nullable = false)
    private LocalDateTime creationDate;

    @Column
    private Integer postalCode;

    @Column
    private double latitude;

    @Column
    private double longitude;

    //  RELATIONS

    @JsonBackReference
    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    private List<CommentOfPost> commentOfPostList = new ArrayList<>();

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "poster")
    private Person person;

    @JsonBackReference
    @OneToMany(mappedBy = "post", cascade = CascadeType.ALL)
    private Set<PostRating> postRatingSet = new HashSet<>();

    //  CONSTRUCTOR

    protected Post(){}

    protected Post(String title, String text, Categories category, LocalDateTime creationDate, Integer postalCode, double latitude, double longitude) {
        this.title = title;
        this.text = text;
        this.category = category;
        this.creationDate = creationDate;
        this.postalCode = postalCode;
        this.latitude = latitude;
        this.longitude = longitude;
        this.approvalModerator = null;
        this.image = "";
    }

    //  GETTER & SETTER & ADDER

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImage() {
        if(image == null || image.equals(""))
            return "http://localhost:8080/standart-post-photos/stockPost.jpg";
        return "http://localhost:8080/post-photos/" + id + "/" + image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(boolean approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovalModerator(){ return approvalModerator;}

    public void setApprovalModerator(String approvalModerator){
        this.approvalModerator = approvalModerator;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public void addLike(){this.likes++;}

    public void deleteLike(){this.likes--;}

    public int getDislikes() {
        return dislikes;
    }

    public void addDislikes(){this.dislikes++;}

    public void deleteDislike(){this.dislikes--;}

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public Categories getCategory() {
        return category;
    }

    public void setCategory(Categories category) {
        this.category = category;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Integer getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(Integer postalCode) {
        this.postalCode = postalCode;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUsername(){
        return person.getUsername();
    }

    //  RELATIONS GETTER & SETTER & ADDER

    public List<CommentOfPost> getCommentOfPostList() {
        return commentOfPostList;
    }

    public void setCommentOfPostList(List<CommentOfPost> commentOfPostList) {
        this.commentOfPostList = commentOfPostList;
    }

    public void addCommentOfPost(CommentOfPost commentOfPost){this.commentOfPostList.add(commentOfPost);}

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Set<PostRating> getPostRatingSet() {
        return postRatingSet;
    }

    public void addPostRating(PostRating postRating){this.postRatingSet.add(postRating);}

    public void setPostRatingSet(Set<PostRating> postRatingSet) {
        this.postRatingSet = postRatingSet;
    }
}
