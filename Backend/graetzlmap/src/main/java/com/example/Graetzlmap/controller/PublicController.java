package com.example.Graetzlmap.controller;
import com.example.Graetzlmap.dTOs.RegisterDTO;
import com.example.Graetzlmap.exitDTOs.MapPostWithCommentsDTO;
import com.example.Graetzlmap.model.MapPost;
import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.services.LoginAndRegisterService;
import com.example.Graetzlmap.services.MapPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/public")
public class PublicController {
    @Autowired
    MapPostService mapPostService;
    @Autowired
    LoginAndRegisterService loginAndRegisterService;

    @GetMapping("/allApprovedPosts")
    public List<MapPost> allApprovedPosts() {
        return mapPostService.findAllApprovedPosts();
    }

    @GetMapping("/login")
    public Person login() {
        return loginAndRegisterService.login();
    }

    @PostMapping("/register")
    public Person register(@RequestBody RegisterDTO registerDTO) throws ResponseStatusException {
        return loginAndRegisterService.register(registerDTO);
    }

    @GetMapping("/logout")
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);

    }

    @GetMapping("/mapPost/{id}")
    public MapPostWithCommentsDTO findMapPost(@PathVariable Integer id) throws ResponseStatusException{return mapPostService.mapPostWithApprovedComments(id);}

}
