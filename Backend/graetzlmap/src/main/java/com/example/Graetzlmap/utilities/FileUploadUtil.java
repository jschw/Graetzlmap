package com.example.Graetzlmap.utilities;
import org.springframework.web.multipart.MultipartFile;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
/**             Method:     uploads image to database and
 *                          saves file name in repository
 *
 *              Exception: Personally I do not know which
 *                         exception would get thrown but in
 *                         the documentation for this version
 *                         of picture upload it is recommended
 *                         via documentation.
 */
public class FileUploadUtil {
    public static void saveFile(String uploadDir, String fileName, MultipartFile multipartFile) throws Exception{
        Path uploadPath = Paths.get(uploadDir);
        if(!Files.exists(uploadPath))
            Files.createDirectories(uploadPath);
        try(InputStream inputStream = multipartFile.getInputStream()){
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        }catch (Exception e){
            throw new Exception("Could not save image file");
        }
    }
}
