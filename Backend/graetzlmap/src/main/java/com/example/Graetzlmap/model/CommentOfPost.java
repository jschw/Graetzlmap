package com.example.Graetzlmap.model;
import com.fasterxml.jackson.annotation.JsonBackReference;
import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "commentOfPost")
@Table
public class CommentOfPost {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(length = 1000)
    private String text;

    @Column
    private boolean approvalStatus;

    @Column
    private LocalDateTime creationDate;

    @Column
    private String approvalModerator;

    //  RELATIONS

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "Person_id")
    private Person person;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name = "Post_id")
    private Post post;

    //  CONSTRUCTOR

    public CommentOfPost(){}

    public CommentOfPost(String text, boolean approvalStatus, LocalDateTime creationDate){
        this.text = text;
        this.approvalStatus = approvalStatus;
        this.creationDate = creationDate;
        this.approvalModerator = null;
    }

    //  GETTER & SETTER

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(boolean approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public String getApprovalModerator() {
        return approvalModerator;
    }

    public void setApprovalModerator(String approvalModerator) {
        this.approvalModerator = approvalModerator;
    }

    public String getUsername() {
        return person.getUsername();
    }

//  RELATIONS GETTER & SETTER & ADDER

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }
}
