package com.example.Graetzlmap.security;

import com.example.Graetzlmap.model.Person;
import com.example.Graetzlmap.repositories.PersonCRUDRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.ArrayList;

@EnableWebSecurity
@Configuration
public class MyUserDetailService implements UserDetailsService {
    @Autowired
    private PersonCRUDRepository personCRUDRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Person person = personCRUDRepository.findByEmail(email);
        if(person==null){
            throw new UsernameNotFoundException("No user found with email:" +email);
        }
        ArrayList<GrantedAuthority> authorities = new ArrayList<>();

        //hard coding a role for every user. (could also be retrieved from the database for advanced authority-management)
        switch (person.getRole()) {
            case ENDUSER -> authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
            case MODERATOR -> authorities.add(new SimpleGrantedAuthority("ROLE_MODERATOR"));
            case ADMIN -> authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }

        //Create spring security user with values from our userin
        return new User(person.getUsername(),(person.getPassword()),authorities);
    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

}
