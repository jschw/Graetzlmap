package com.example.Graetzlmap.controller;
import com.example.Graetzlmap.dTOs.CommentOfPostChangeDTO;
import com.example.Graetzlmap.dTOs.CommentOfPostDTO;
import com.example.Graetzlmap.model.CommentOfPost;
import com.example.Graetzlmap.services.CommentOfPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/commentOfPost")
public class CommentOfPostController {
    @Autowired
    CommentOfPostService commentOfPostService;

    @PostMapping("")
    public CommentOfPost createCommentOfPost(@RequestBody CommentOfPostDTO commentOfPostDTO) throws ResponseStatusException {return commentOfPostService.createNewCommentOfPost(commentOfPostDTO);}

    @GetMapping("/all")
    public List<CommentOfPost> allCommentsOfPosts(){return commentOfPostService.findAll();}

    @GetMapping("/{id}")
    public CommentOfPost findCommentOfPost(@PathVariable int id) throws ResponseStatusException{return commentOfPostService.findCommentOfPost(id);}

    @DeleteMapping("/{id}")
    public void deleteCommentOfPost(@PathVariable Integer id) throws ResponseStatusException{commentOfPostService.deleteCommentOfPost(id);}

    @PutMapping("")
    public void updateCommentOfPost(CommentOfPostChangeDTO commentOfPostChangeDTO) throws ResponseStatusException{commentOfPostService.updateCommentOfPost(commentOfPostChangeDTO);}


}
