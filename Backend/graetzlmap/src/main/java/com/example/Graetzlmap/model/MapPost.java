package com.example.Graetzlmap.model;
import com.example.Graetzlmap.enums.Categories;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity(name = "mapPost")
@Table
public class MapPost extends Post{

    public MapPost(){}

    public MapPost(String title, String text, Categories category, LocalDateTime creationDate, Integer postalCode, double latitude, double longitude){
        super(title, text,  category, creationDate, postalCode, latitude, longitude);
        this.setApprovalStatus(false);
    }
}
