package com.example.Graetzlmap.dTOs;

public class PostRatingDTO {
    private Boolean rating;
    private Integer postId;

    public PostRatingDTO(Boolean rating, Integer postId) {
        this.rating = rating;
        this.postId = postId;
    }

    public Boolean isRating() {
        return rating;
    }

    public void setRating(Boolean rating) {
        this.rating = rating;
    }

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }
}
