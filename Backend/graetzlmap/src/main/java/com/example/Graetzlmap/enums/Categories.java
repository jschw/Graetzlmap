package com.example.Graetzlmap.enums;

public enum Categories {
    GRÜNFLÄCHEN, NUTZFLÄCHEN, VERKEHRSSICHERHEIT, WEGE, AKTIVITÄTEN, SONSTIGES
}
