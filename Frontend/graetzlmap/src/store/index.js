import Vue from 'vue'
import Vuex from 'vuex'
//import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,


    //format: [{id, person, latitude, longitude, title, text, img, category, date, zip, verified,}]
    posts: null,

    illegalPosts: null,

    snackbars: [],

    userList: null,

    illegalComments: null,

    districts: [{id: 1, name: "Innere Stadt", zip: 1010},
                {id: 2, name: "Leopoldstadt", zip: 1020},
                {id: 3, name: "Landstraße", zip: 1030},
                {id: 4, name: "Wieden", zip: 1040},
                {id: 5, name: "Margareten", zip: 1050},
                {id: 6, name: "Mariahilf", zip: 1060},
                {id: 7, name: "Neubau", zip: 1070},
                {id: 8, name: "Josefstadt", zip: 1080},
                {id: 9, name: "Alsergrund", zip: 1090},
                {id: 10, name: "Favoriten", zip: 1100},
                {id: 11, name: "Simmering", zip: 1110},
                {id: 12, name: "Meidling", zip: 1120},
                {id: 13, name: "Hietzing", zip: 1130},
                {id: 14, name: "Penzing", zip: 1140},
                {id: 15, name: "Fünfhaus", zip: 1150},
                {id: 16, name: "Ottakring", zip: 1160},
                {id: 17, name: "Hernals", zip: 1170},
                {id: 18, name: "Währing", zip: 1180},
                {id: 19, name: "Döbling", zip: 1190},
                {id: 20, name: "Brigittenau", zip: 1200},
                {id: 21, name: "Floridsdorf", zip: 1210},
                {id: 22, name: "Donaustadt", zip: 1220},
                {id: 23, name: "Liesing", zip: 1230}],

    currentPost:null,

    currentLat:null,
    currentLong:null,            

  },
  getters: {

    postById(state){
      //postId is handed over via the parameter from the component
      return postId => {
        return state.posts.find(post => post.id == postId)
      }
    },
    
    postsByCategory(state){
      return category => {
        return state.posts.filter(post => post.category == category)
      }
    },

    postsByDistrict(state){
      return district => {
        return state.posts.filter(post => post.postalCode == district)
      }
    },

    postsByDateAsc(state){
      //alternatively we could just use .reverse()
      if(state.posts){
        const postsByDateAsc = [...state.posts].sort((a, b) => Date.parse(a.creationDate) - Date.parse(b.creationDate))
        return postsByDateAsc
      }
    },

    postsByLikes(state){
      if(state.posts){
        const postsByLikes = [...state.posts].sort((a, b) => b.likes - a.likes)
        return postsByLikes
      }
    },

    postsByDislikes(state){
      if(state.posts){
        const postsByDislikes = [...state.posts].sort((a, b) => b.dislikes - a.dislikes)
        return postsByDislikes
      }
    },
  
    illegalPostsByDateAsc(state){
      if(state.illegalPosts){
        const postsByDateAsc = [...state.illegalPosts].sort((a, b) => Date.parse(a.creationDate) - Date.parse(b.creationDate))
        return postsByDateAsc
      }
    },

    illegalPostsByLikes(state){
      if(state.illegalPosts){
        const postsByLikes = [...state.illegalPosts].sort((a, b) => b.likes - a.likes)
        return postsByLikes
      }
    },

    illegalPostsByDislikes(state){
      if(state.illegalPosts){
        const postsByDislikes = [...state.illegalPosts].sort((a, b) => b.dislikes - a.dislikes)
        return postsByDislikes
      }
  },

  },

  mutations: {
    setUser (state, user) {
      state.user = user
    },
    setPosts (state, posts) {
      state.posts = posts.reverse()
    },
    setIllegalPosts (state, posts) {
      state.illegalPosts = posts.reverse()
    },
    setCurrentPost(state, currentPost){
      state.currentPost = currentPost
    },
    setIllegalComments (state, comments) {
      state.illegalComments = comments
    },

    setUserList (state, users) {
      state.userList = users
    },

    setSnackbar(state, snackbar) {
      state.snackbars = state.snackbars.concat(snackbar)
    },

    removeComment(state, credentials) {
      if(credentials.route == 'AdminCommentsList')
        state.illegalComments = state.illegalComments.filter(comment => comment.id !== credentials.id)
      else if(credentials.route == 'SinglePostView')
        state.currentPost.commentOfPostList = state.currentPost.commentOfPostList.filter(comment => comment.id !== credentials.id)
    },

    updateComments(state, comment) {
      comment.approvalStatus = true
      state.currentPost.commentOfPostList.push(comment)
    },

    updatePostRatings(state, post) {
      state.currentPost.likes = post.likes
      state.currentPost.dislikes = post.dislikes
    },

    updateApproval (state, credentials) {
      if(credentials.route === 'AdminCommentsList'){
        state.illegalComments.find(comment => comment.id === credentials.id).approvalStatus = credentials.approvalStatus
      }else{
        state.currentPost.commentOfPostList.find(comment => comment.id === credentials.id).approvalStatus = credentials.approvalStatus
      }

    },

    removeUser: (state, username) => state.userList = state.userList.filter(user => user.username !== username),

    updateRole: (state, credentials) => state.userList.find(user => user.username === credentials.username).role = credentials.role,

  },

  actions: {

    //USER REQUESTS//
    /////////////////

    //LOGIN
    async login (context, credentials) {
      var basicAuth = 'Basic ' + btoa(credentials.email + ':' + credentials.password)
      const response = await Vue.axios.get('/api/public/login', {
        headers: { 'Authorization': basicAuth }
      })
      context.commit('setUser', response.data)
    },

    //REGISTER
    async register (context, newUserData) {
      const response = await Vue.axios.post('/api/public/register', newUserData)
      context.commit('setUser', response.data)
    },

    //RELOAD USER-COOKIE GETTER
    async getSignedInUser (context) {
      const response = await Vue.axios.get('/api/public/login')
      context.commit('setUser', response.data)
    },

    //LOGOUT
    async logout (context) {
      await Vue.axios.get('/api/public/logout')
      context.commit('setUser', null)
    },

    //CHANGE - USER SETTINGS
    async putUser(context, user) {
      const response = await Vue.axios.put('/api/user/changeSettings', user)
      context.commit('setUser', response.data)
    },
    

    //MAP-POST REQUESTS//
    /////////////////

    //POST - NEW MAP POST
    async newMapPost(context, credentials){
      const response = await Vue.axios.post('/api/mapPost', credentials)
      return response.data
    },

    //GET - ALL APPROVED POSTS WITHOUT LOGIN
    async fetchPosts(context){
      const response = await Vue.axios.get('/api/public/allApprovedPosts')
      context.commit('setPosts', response.data)
    },

    //GET - ALL POSTS OF A SPECIFIC USER
    async fetchPostsOfUser(context, username){
      const response = await Vue.axios.get('/api/public/mapPosts/' + username)
      context.commit('setPosts', response.data)
    },

    //GET - SPECIFIC MAP POST WITH POST ID
    async getUserPostData(context, id){
      const response = await Vue.axios.get('/api/public/mapPost/' + id)
      context.commit('setCurrentPost', response.data)
    },

    //POST - COMMENT
    async postComment(context, credentials){
      const response = await Vue.axios.post('/api/commentOfPost', credentials)
      if (this.state.user && (this.state.user.role === 'ADMIN' || this.state.user.role === 'MODERATOR'))
        context.commit('updateComments', response.data)
    },

    //LIKE / DISLIKE REQUESTS
    async ratePost(context, postRatingDTO){
      const response = await Vue.axios.post('/api/postRating', postRatingDTO)
      context.commit('updatePostRatings', response.data)
    },

    //LIKE / DISLIKE USERS-LIKE-OF-POST
    async getSingleRating(context, postId){
      const response = await Vue.axios.get('/api/postRating/' + postId)
      return response.data
    },


    //ADMIN/POST+COMMENT ADMINISTRATION//
    /////////////////////////////////////

    
    //GET - SPECIFIC MAP POST WITH POST ID
    async getAdminPostData(context, id){
      const response = await Vue.axios.get('/api/admin/postWithAllComments/' + id)
      context.commit('setCurrentPost', response.data)
    },

    //DELETE - DELETE POST BY ID
    async deletePostById(context, id){
      const response = await Vue.axios.delete('/api/mapPost/' + id)
      context.commit('setPosts', response.data)
   },
   
    //DELETE - DELETE COMMENT BY ID
    async deleteCommentById(context, credentials){
      await Vue.axios.delete('/api/commentOfPost/' + credentials.id)
      context.commit('removeComment', credentials)
    },
       
    //DELETE - DELETE USER BY ID
    async deleteUser(context, username){
      await Vue.axios.delete('/api/user/' + username)
      context.commit('removeUser', username)
    },
   
    //GET - ALL POSTS (approved and not approved)
    async fetchAllPosts(context){
      const response = await Vue.axios.get('/api/mapPost/all')
      context.commit('setPosts', response.data)
    },

    //GET - ALL NON-APPROVED POSTS
    async fetchIllegalPosts(context){
      const response = await Vue.axios.get('/api/admin/allIllegalPosts')
      context.commit('setIllegalPosts', response.data)
    },

    //GET - ALL NON-APPROVED COMMENTS
    async fetchIllegalComments(context){
      const response = await Vue.axios.get('/api/admin/allIllegalComments')
      context.commit('setIllegalComments', response.data)
    },

    //PUT - CHANGE MAPPOST APPROVAL-STATUS
    async changePostApproval(context, id){
      await Vue.axios.put('/api/admin/postApproval/' + id)
    },
    
    //PUT - CHANGE COMMENT APPROVAL-STATUS
    async changeCommentApproval(context, credentials){
      const response = await Vue.axios.put('/api/admin/commentApproval/' + credentials.id)
      credentials.approvalStatus = response.data
      context.commit('updateApproval', credentials)
    },
        
    //PUT - PROMOTE ENDUSER TO MODERATOR
    async promoteUser(context, username){
      await Vue.axios.put('/api/admin/personToModerator/' + username)
      context.commit('updateRole', {username, role:'MODERATOR'})

    },
        
    //PUT - DEMOTE MODERATOR TO ENDUSER
    async demoteUser(context, username){
      await Vue.axios.put('/api/admin/personToEndUser/' + username)
      context.commit('updateRole', {username, role:'ENDUSER'})
    },

    //GET - ALL USERS
    async fetchAllUsers(context){
      const response = await Vue.axios.get('/api/user/all')
      context.commit('setUserList', response.data)
    },


    //PICTURE UPLOAD REQUESTS//
    ///////////////////////////

    //POST - PROFILE-PICTURE UPLOAD
    async profilePictureUpload(context, image){
        const response = await Vue.axios.post('/api/user/profilePicture', image)
        context.commit('setUser', response.data)
    },

    //POST  - MAP-POST-PICTURE UPLOAD
    async postPictureUpload(context, image){
      await Vue.axios.post('/api/mapPost/picture/' , image)
    },

    //SNACKBARS//
    /////////////
    getSnackbar(context, snackbar) {
      snackbar.showing = true
      snackbar.timeout = snackbar.color==='error'?-1:4000
      snackbar.color = snackbar.color || 'primary'
      context.commit('setSnackbar', snackbar)
    }
  }
})
