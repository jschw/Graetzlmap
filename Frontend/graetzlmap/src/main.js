import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import router from './router'
import store from './store'
import VueAxios from 'vue-axios'
import axios from 'axios'
import vuetify from './plugins/vuetify'
import Vuelidate from 'vuelidate'
import './registerServiceWorker'



Vue.use(Vuelidate)
Vue.use(VueRouter)
Vue.use(VueAxios, axios)

Vue.config.productionTip = false


//prevents getting logged-out after reload(?)
async function createVueApp() {
  await store.dispatch('getSignedInUser')

  new Vue({
    router,
    store,
    vuetify,
    render: h => h(App)
  }).$mount('#app')
}
createVueApp()

