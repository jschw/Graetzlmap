import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi'
      },
    theme: {
        themes: {
            light: {
                primary: '#8DA290',
                secondary: '#FCF1D8',
                accent: '#F2CBBB',
                error: '#FF6961'
            }
        }
    }
});
