import Vue from 'vue'
import VueRouter from "vue-router"
import store from '@/store'


Vue.use(VueRouter)

const routes = [
    {
        path: "/",
        name: "LandingPage",
        component: () => import('@/views/LandingPage')
    },
    {
        path: '/anmelden',
        component: () => import('@/layouts/AuthLayout'),
        beforeEnter (to, from, next) {
            if (!store.state.user) {
              return next()
            }
            return next('/bereitseingeloggt')
        },
        children: [
            {
            path: '',
            name: 'Login',
            component: () => import('@/components/Login')
            },
            {
            path: '/registrieren',
            name: 'Register',
            component: () => import('@/components/Register')
            }
        ],
    },
    {
        path: "/karte",
        name: "MapView",
        component: () => import('@/views/MapView')
    },
    {
        path: "/erstellen",
        name: "PostCreate",
        component: () => import('@/views/PostsCreate'),
        beforeEnter (to, from, next) {
            if (store.state.user) {
              return next()
            }
            return next('/anmelden')
        }
    },
    {
        path: "/eintraege",
        name: "PostsList",
        component: () => import('@/views/PostsList'),
        beforeEnter (to, from, next) {
            //required because onCreate-fetch doesn't fire coming from the following two routes as all of them are PostList.vue
            if (from.name === "CategorysPosts" || from.name === "DistrictsPosts") {
                store.state.posts = null
                store.dispatch('fetchPosts')
                return next()
            }
            return next()
        }
    },
    {
        path: "/kategorien",
        name: "PostsCategories",
        component: () => import('@/views/PostsCategories')
    },
    {
        path: "/kategorien/:category",
        name: "CategorysPosts",
        component: () => import('@/views/PostsList')
    },
    {
        path: "/bezirke",
        name: "PostsDistricts",
        component: () => import('@/views/PostsDistricts')
    },
    {
        path: "/bezirke/:district",
        name: "DistrictsPosts",
        component: () => import('@/views/PostsList')
    },
    {
        path: "/eintrag:id",
        name: "SinglePostView",
        component: () => import('@/views/SinglePostView')
    },
    {
        path: "/verwaltung/eintraege",
        name: "AdminPostsList",
        component: () => import('@/views/AdminPostsList'),
        beforeEnter (to, from, next) {
            if (store.state.user.role === 'ADMIN' ||
                store.state.user.role === 'MODERATOR') {
                return next()
            }
            return next('/keinzutritt')
        }
    },
    {
        path: "/verwaltung/kommentare",
        name: "AdminCommentsList",
        component: () => import('@/views/AdminCommentsList'),
        beforeEnter (to, from, next) {
            if (store.state.user.role === 'ADMIN' ||
                store.state.user.role === 'MODERATOR') {
                return next()
            }
            return next('/keinzutritt')
        }
    },
    {
        path: "/verwaltung/mitglieder",
        name: "AdminUserList",
        component: () => import('@/views/AdminUserList'),
        beforeEnter (to, from, next) {
            if (store.state.user.role === 'ADMIN') {
              return next()
            }
            return next('/keinzutritt')
        }
    },
    {
        path: "/profil:id",
        name: "UserProfile",
        component: () => import('@/views/UserProfile'),
        beforeEnter (to, from, next) {
            if (store.state.user) {
              return next()
            }
            return next('/anmelden')
        }
    },
    {
        path: "/nutzungsbedingungen",
        name: "ToS",
        component: () => import('@/views/TermsOfUse')
    },
    {
        path: "/team",
        name: "Team",
        component: () => import('@/views/Team')
    },
    {
        path: "/contact",
        name: "Contact",
        component: () => import('@/views/Contact')
    },
    {
        path: "/keinzutritt",
        name: "CannotEnter",
        component: () => import('@/views/CannotEnter')
    },
    {
        path: "/bereitseingeloggt",
        name: "AlreadySignedIn",
        component: () => import('@/views/AlreadySignedIn')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
    })
    
    export default router
    